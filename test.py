import requests
from bs4 import  BeautifulSoup
course_term_headers = {
    'authority':'coursehome.zhihuishu.com',
    'accept':'application/json, text/javascript, */*; q=0.01',
    'x-requested-with':'XMLHttpRequest',
    'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63',
    'content-type':'application/x-www-form-urlencoded; charset=UTF-8',
    'origin':'https://coursehome.zhihuishu.com',
    'sec-fetch-site':'same-origin',
    'sec-fetch-mode':'cors',
    'sec-fetch-dest':'empty',
    'referer':'https://coursehome.zhihuishu.com/courseHome/1000006450/8096/10?state=1',
    'accept-language':'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'cookie':'Hm_lvt_0a1b7151d8c580761c3aef32a3d501c6=1612700574; Hm_lpvt_0a1b7151d8c580761c3aef32a3d501c6=1612700574; staus=2; c_session_id=D4B12D1A9011637B381A76E1137A5544; acw_tc=7b605d9816127645820234736e6d722329ef9f3a09c3b4eb4915274c7a; JSESSIONID=AF27A7B8C0E19E7B5639CD235EC749AE; SERVERID=472b148b148a839eba1c5c1a8657e3a7|1612766021|1612766021'
}
course_term_params = {
    'courseId':'1000006450',
    'termId':'10',
    'recruitId':'8096',
    'recruitIds[0]':'8096'
}
request = requests.post(url='https://coursehome.zhihuishu.com/home/pageCount',
                       headers = course_term_headers,
                       params=course_term_params)
request.encoding = request.apparent_encoding
print(BeautifulSoup(request.text,'html.parser').prettify())