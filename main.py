# # This is a sample Python script.
#
# # Press Shift+F10 to execute it or replace it with your code.
# # Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
#
#
# def print_hi(name):
#     # Use a breakpoint in the code line below to debug your script.
#     print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
#
#
# Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     pass
#     # print_hi('PyCharm')

# # See PyCharm help at https://www.jetbrains.com/help/pycharm/
'''
load_brief_information:
    courseId    int
    credit  float
    runTermCount    int
    studentCount    int
    hours   int


课程设计是不变的
互动问答会随着学期的变化而变化
教学团队是不变的

'''
import requests
from datetime import datetime
import math
import json
from bs4 import BeautifulSoup
import re
import os
course_list_headers = {
    'Connection':'keep-alive',
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.146 Safari/537.36 Edg/88.0.705.62',
    'Accept':'*/*',
    'Origin':'https://www.zhihuishu.com',
    'Sec-Fetch-Site':'same-site',
    'Sec-Fetch-Mode':'cors',
    'Sec-Fetch-Dest':'empty',
    'Referer':'https://www.zhihuishu.com/',
    'Accept-Language':'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'Cookie':'Hm_lvt_0a1b7151d8c580761c3aef32a3d501c6=1612609343; Hm_lpvt_0a1b7151d8c580761c3aef32a3d501c6=1612609372; acw_tc=2f624a3216126093661703776e61d5944ec1cdea09d016305fb8c8274c86e9; SERVERID=4516a494fea80bcfc392e5b45dea0690|1612609384|1612609366'
}
course_list_params = {
    'family':'-1',
    'coursePackageId':'-1',
    'zhsmCourseCategory':'-1',
    'subjectFirstCode':'-1',
    'schoolLevel':'-1',
    'orderRuler':'-1',
    # pageNo从0-112,
    'pageSize':'60',
    'zhsmCourseCategoryTag':'-1',
    # 'dateFormate'=当前时间
    'uuid':''
}
course_list_limit = 113
course_page_url = 'https://coursehome.zhihuishu.com/courseHome/{courseId}#teachTeam'
COURSE_BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)),'course')

course_headers = {
    'authority':'coursehome.zhihuishu.com',
    'accept':'application/json, text/javascript, */*; q=0.01',
    'x-requested-with':'XMLHttpRequest',
    'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63',
    'content-type':'application/x-www-form-urlencoded; charset=UTF-8',
    'origin':'https://coursehome.zhihuishu.com',
    'sec-fetch-site':'same-origin',
    'sec-fetch-mode':'cors',
    'sec-fetch-dest':'empty',
    'referer':'https://coursehome.zhihuishu.com/courseHome/1000006450/8096/10?state=1',
    'accept-language':'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'cookie':'Hm_lvt_0a1b7151d8c580761c3aef32a3d501c6=1612700574; Hm_lpvt_0a1b7151d8c580761c3aef32a3d501c6=1612700574; staus=2; c_session_id=D4B12D1A9011637B381A76E1137A5544; acw_tc=7b605d9816127645820234736e6d722329ef9f3a09c3b4eb4915274c7a; JSESSIONID=AF27A7B8C0E19E7B5639CD235EC749AE; SERVERID=472b148b148a839eba1c5c1a8657e3a7|1612766021|1612766021'
}
course_term_params = {
    'courseId':'1000006450',
    'termId':'10',
    'recruitId':'8096',
    'recruitIds[0]':'8096'
}
query_params = {
    'courseId':'1000006644',
    'termId':'12',
    'recruitId':'14761',
    'quoteRecruitIds':'',
    'teacherJoin':'false',
    'pageNum':'1'
}
answer_params = {
    'questiontitle':'儿童为什么需要游戏？儿童酷爱游戏。幼儿园也将游戏作为儿童的基本活动。讨论：儿童为什么需要游戏？&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;游戏对儿童的发展有什么价值和意义？',
    'replyCount':'24',
    'currentPage':'1',
    'questionId':'366070662',
    'recruitId':'14761',
    'termId':'12'
}
####
new_course = True  # 是否是一节新的课程
hasTerms = False # 是否有多个学期
###


def exception_dealing():
    print('Exception')
    result = input()

def get_course_list():
    '''
    获得一页的课程列表
    :return:
    '''
    for i in range(course_list_limit):
        course_list_params['dateFormate'] = str(math.floor(datetime.now().timestamp() * 1000))
        course_list_params['pageNo'] = str(i)
        request =  requests.get(url='https://bd-search.zhihuishu.com/bg_search/indexPageSearch/shareCourse',
                                headers = course_list_headers,
                                params=course_list_params)
        request_dictionary = json.loads(request.text)
        yield request_dictionary['rt']
    return None

def load_brief_information(course):
    '''
    记录课程列表页面的基础信息
    :param course:
    :param course_dictionary:
    :return:
    '''
    try:
        # course_dictionary['courseId'] = course['courseId']
        # course_dictionary['courseName'] = course['courseName']
        # course_dictionary['schoolName'] = course['schoolName']
        # course_dictionary['lectorName'] = course['teacherName']  # 核心教师
        # course_dictionary['credit'] = course['credit']
        # course_dictionary['runTermCount'] = course['runTermCount']
        # course_dictionary['coursePackageName'] = course['coursePackageName']
        # course_dictionary['studentCount'] = course['studentCount']
        # course_dictionary['fromType'] = course['fromType']
        # course_dictionary['courseModeName'] = course['courseModeName']
        # course_dictionary['hours'] = course['hours']
        course_dictionary = course
        course_dictionary['courseId'] = str(course_dictionary['courseId'])
        return course_dictionary
    except:
        exception_dealing()


def get_details(response,course_dictionary):
    try:
        soup = BeautifulSoup(response.text, 'html.parser')
        pre_list = soup.select('div[class = "course-design-list clearfix" ] ul pre')
        course_dictionary['course_details'] = {
            'background': str(pre_list[0].string),
            'target': str(pre_list[1].string),
            'design principle': str(pre_list[2].string)
        }
    except:
        pass


def get_course_details(course_dictionary):
    global hasTerms
    if hasTerms:
        url = 'https://coursehome.zhihuishu.com/home/courseDesign/querycontent/{courseId}/{recruitId}'.format(
            courseId = course_dictionary['courseId'],
            recruitId = course_dictionary['terms'][0]['recruitId']
        )
    else:
        url = 'https://coursehome.zhihuishu.com/home/courseDesign/querycontent/{courseId}/'.format(
            courseId=course_dictionary['courseId']
        )
    response = requests.post(url=url, headers=course_headers)
    get_details(response,course_dictionary)

def get_course_term_information(course_dictionary):
    global new_course
    for term in course_dictionary['terms']:
        course_term_params['termId'] = term['termId'],
        course_list_params['recruitId'] = term['recruitId'],
        course_term_params['recruitIds[0]'] = term['recruitId'],
        request = requests.post(url='https://coursehome.zhihuishu.com/home/pageCount',
                                headers = course_headers,
                                params = course_term_params)
        term_dictionary = json.loads(request.text)
        if new_course:
            course_dictionary['bbsQaTotalCount'] = term_dictionary['bbsQaTotalCount']
            course_dictionary['personTotalCount'] = term_dictionary['personTotalCount']
            course_dictionary['schoolTotalCount'] = term_dictionary['schoolTotalCount']
            new_course = False
        term['bbsQaCount'] = term_dictionary['bbsQaCount']
        term['schoolCount'] = term_dictionary['schoolCount']
        term['personCount'] = term_dictionary['personCount']
        try:
            term['recruitRunningDto'] = term_dictionary['recruitRunningDto']
        except:
            pass

def get_query_answers(term,query_dictionary,course_id):
    url = 'https://coursehome.zhihuishu.com/home/discuss/queryQuestionAnswerList/{queryId}/{courseId}'.format(
        courseId = course_id,
        queryId = query_dictionary['queryId']
    )
    answer_params['questiontitle'] = query_dictionary['queryTitle']
    answer_params['questionId'] = query_dictionary['queryUserId']
    answer_params['replyCount'] = query_dictionary['queryReplyCount']
    answer_params['recruitId'] = term['recruitId']
    answer_params['termId'] = term['termId']
    query_dictionary['replies'] = []
    index = 0
    while True:
        index += 1
        replies = []
        answer_params['currentPage'] = str(index)
        response = requests.post(url = url,headers = course_headers,params = answer_params)
        soup = BeautifulSoup(response.text,'html.parser')
        answer_name_span_list = soup.select('span[class = "fl" ]')
        answer_li_list = soup.select('ul[class = "Answerquestionscommentlist-ul" ]>li')
        answer_time_span_list = soup.select('span[class = "questionanswertime-span fr" ]')
        answer_description_div_list = soup.select('div[class = "questiondescribe-div" ]')
        if len(answer_li_list) == 0:
            break
        for i in range(len(answer_li_list)):
            reply_dictionary = {
                'replyUser':str(answer_name_span_list[i].string),
                'replyTime':str(answer_time_span_list[i].string),
                'replyDescription':str(answer_description_div_list[i].string),
                'replyId':answer_li_list[i]['answerid']
            }
            replies.append(reply_dictionary)
        query_dictionary['replies'].extend(replies)

def get_querys(course_dictionary):
    url = 'https://coursehome.zhihuishu.com/home/discuss/qaquestionlist'
    for term in course_dictionary['terms']:
        query_params['termId'] = term['termId']
        query_params['recruitId'] = term['recruitId']
        index = 0
        term['query'] = []
        while True:
            index += 1
            query_params['pageNum'] = str(index)
            response = requests.post(url=url,headers=course_headers,params = query_params)
            soup = BeautifulSoup(response.text,'html.parser')
            query_name_span_list = soup.select('span[class = "fl" ]')
            query_time_span_list = soup.select('span[class = "questionanswertime-span fr" ]')
            query_record_li_list = soup.select('div[class = "operationdata-div" ] ul li')
            query_title_div_list = soup.select('div[class = "questiondescribe-div questiontitle" ]')
            query_li_list = soup.select('li[class = "questionli" ]')
            query_list = []
            if len(query_name_span_list) == 0:
                break
            for i in range(len(query_name_span_list)):
                query_dictionary = {
                    'queryUser':str(query_name_span_list[i].string),
                    'queryTime':str(query_time_span_list[i].string),
                    'queryWatched':re.findall(r'\d+',str(query_record_li_list[i*3].string))[0],
                    'queryPassing_by':re.findall(r'\d+',str(query_record_li_list[i*3+1].string))[0],
                    'queryReplyCount':query_record_li_list[i*3+2]['replycount'],
                    'queryTitle':str(query_title_div_list[i].string),
                    'queryId':query_li_list[i]['qid'],
                    'queryUserId':query_li_list[i]['userid']
                }
                if int(query_dictionary['queryReplyCount'])>0:
                    get_query_answers(term,query_dictionary,course_dictionary['courseId'])
                query_list.append(query_dictionary)
            term['query'].extend(query_list)



def get_teachers(response,course_dictionary):
    soup = BeautifulSoup(response.text,'html.parser')
    course_dictionary['teachers'] = []
    span_list = soup.select("span")
    pre_list = soup.select("pre")
    p_list = soup.select("p")
    for i in range(len(pre_list)):
        teacher_dictionary = {
            'teacherName':str(span_list[i*5+1].string),
            'teacherSchool':str(span_list[i*5+3].string),
            'role':str(span_list[i*5+4].string),
            'title':str(p_list[i].string),
            'introduction':str(pre_list[i].string)
        }
        course_dictionary['teachers'].append(teacher_dictionary)

def get_course_teachers(course_dictionary):
    global hasTerms
    if hasTerms:
        url = 'https://coursehome.zhihuishu.com/home/teachTeam/teamData/{courseId}/{recruitId}'.format(
            courseId=course_dictionary['courseId'],
            recruitId=course_dictionary['terms'][0]['recruitId']
        )
    else:
        url = 'https://coursehome.zhihuishu.com/home/teachTeam/teamData/{courseId}/'.format(
            courseId=course_dictionary['courseId']
        )
    response = requests.post(url=url,headers = course_headers)
    get_teachers(response,course_dictionary)

def get_standard(response,term):
    try:
        soup = BeautifulSoup(response.text, 'html.parser')
        dt_list = soup.select('dt')
        span_list = soup.select('dd span')
        term['standard'] = {}
        for i in range(len(dt_list)):
            term['standard'][str(dt_list[i].text)] = str(span_list[i].string)
    except:
        pass


def get_term_standard(course_dictionary):
    global hasTerms
    if hasTerms:
        for term in course_dictionary['terms']:
            url = 'https://coursehome.zhihuishu.com/home/assessment/queryContent/{courseId}/{recruitId}/{termId}'.format(
                courseId = course_dictionary['courseId'],
                recruitId = term['recruitId'],
                termId = term['termId']
            )
            response = requests.post(url=url,headers = course_headers)
            get_standard(response,term)
    else:
        url = 'https://coursehome.zhihuishu.com/home/assessment/queryContent/{courseId}//-100'.format(
            courseId = course_dictionary['courseId']
        )
        response = requests.post(url=url,headers = course_headers)
        get_standard(response,course_dictionary)

def get_course_judge(course_dictionary):
    for term in course_dictionary['terms']:
        url = 'https://coursehome.zhihuishu.com/home/operationData/curriculumEvaluation/{courseId}/{recruitId}/{termId}'.format(
            courseId = course_dictionary['courseId'],
            recruitId = term['recruitId'],
            termId = term['termId']
        )
        response = requests.get(url=url,headers = course_headers)
        term['judge'] = {}
        soup = BeautifulSoup(response.text,'html.parser')
        div_word_list = soup.select('div[class = "word" ]')
        span_number_list = soup.select('span[class = "num-number" ]')
        span_word_list = soup.select('span[class = "num-word" ]')
        for i in range(len(div_word_list)):
            term['judge'][str(div_word_list[i].string)]={
                'number':str(span_number_list[i].string),
                'unit':str(span_word_list[i].string)
            }


def get_course_page(course_dictionary):
    global new_course
    global hasTerms
    new_course = True
    hasTerms = False
    request = requests.get(url=course_page_url.format(courseId=course_dictionary['courseId']))
    soup = BeautifulSoup(request.text,'html.parser')
    # 获取课程的类别
    try:
        course_dictionary['category'] = str(soup.select('div[class = "CourseHome-div-all"] '+
                                                  'div[class = "Skinpeelerbackgroundimg-box"] '+
                                                  'div[class = "Skinpeelerbackgroundimg-div txtEllipsis"] '+
                                                  'div[class = "SkinpeelerbackgroundStyle-div"]')[1].string)
    except:
        course_dictionary['category'] = '其他(9999)'
    # 获取见面课次数,如果有的话
    try:
        course_dictionary['meetings'] = str(soup.select('div[class = "CourseHome-div-all"] '+
                                                  'div[class = "courseHome-wrap"] '+
                                                  'div[class = "course-imgintroduction-div clearfix"] '+
                                                  'div[class = "course-imgintroduction-r fl"] '+
                                                  'div[class = "credithourMeetingclass-div"]>ul li:nth-child(3) span')[1].string)
        course_dictionary['meetings'] = int(re.findall(r'\d+',course_dictionary['meetings'])[0])  # 这里只取数字
    except:
        course_dictionary['meetings'] = 0
    # 获取课程的课程介绍
    try:
        course_dictionary['introduction'] = str(soup.select('div[class = "CourseHome-div-all"] '+
                                                  'div[class = "courseHome-wrap"] '+
                                                  'div[class = "course-imgintroduction-div clearfix"] '+
                                                  'div[class = "course-imgintroduction-r fl"] '+
                                                  'div[class = "Courseintroductiondiv"]>p')[0].string)
    except:
        course_dictionary['introduction'] = '暂无介绍'
    course_dictionary['terms'] = []
    # 对于有至少1个学期并且选课人数比较多的同学
    try:
        li_list = soup.select('div[id = "firstLevel"]>'+
                             'div[class = "content"] ul li')
        a_list = soup.select('div[id = "firstLevel"]>'+
                             'div[class = "content"] ul li a')
        span_list = soup.select('div[id = "firstLevel"]>'+
                             'div[class = "content"] ul li a span')
        for i in range(len(li_list)):
            term_dictionary = {
                'termName':str(span_list[i*3+1].string),
                'termStatus':str(span_list[i*3+2].string),
                'termId':li_list[i]['termid'],
                'recruitId':a_list[i]['recruitid'][1:-1]
            }
            course_dictionary['terms'].append(term_dictionary)
        course_headers['referer'] = \
            'https://coursehome.zhihuishu.com/courseHome/{courseId}/{recruitId}/{termId}?state=1'.format(
                courseId=course_dictionary['courseId'],
                recruitId = course_dictionary['terms'][0]['recruitId'],
                termId = course_dictionary['terms'][0]['termId']
            )
        hasTerms = True
        course_term_params['courseId'] = course_dictionary['courseId']
        query_params['courseId'] = course_dictionary['courseId']
        # 获得各个学期的详细信息
        get_course_term_information(course_dictionary)
    except:
        pass
    # 获取课程设计
    if not hasTerms:
        course_headers['referer'] = \
            'https://coursehome.zhihuishu.com/courseHome/{courseId}'.format(
                courseId=course_dictionary['courseId']
            )
    get_course_details(course_dictionary)
    # 获取教师信息
    get_course_teachers(course_dictionary)
    if hasTerms:
        # 获取互动信息
        get_querys(course_dictionary)
        # 获取评审
        get_course_judge(course_dictionary)
    # 获取考核标准
    get_term_standard(course_dictionary)

def file_write(course_dictionary):
    # try:
    path = os.path.join(COURSE_BASE_DIR,course_dictionary['category'])
    if not os.path.exists(path):
        os.makedirs(path)
    file_name = os.path.join(path,course_dictionary['courseName']+'('+course_dictionary['courseId']+').json')
    with open(file_name,'w',encoding='utf-8') as file:
        content = json.dumps(course_dictionary,sort_keys=True,indent='\t',ensure_ascii=False)
        file.write(content)
        file.close()
    # except:
    #     pass




def main():
    for course_list in get_course_list():
        if course_list is not None:
            for course in course_list:
                course_dictionary = load_brief_information(course)
                get_course_page(course_dictionary)
                file_write(course_dictionary)

if __name__ == '__main__':
    main()